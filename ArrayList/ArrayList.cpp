#include <iostream>

class OutOfMemoryException {};
class NotInRangeException {};

class ArrayList {
private:
    int* pointer;
    int current;
    int size;

    void expand(int newSize) {
        int* temp;
        newSize = newSize + this->size / 5;

        temp = (int*)realloc(this->pointer, sizeof(int) * newSize);

        if (DEBUG) {
            std::cout << "expand(int newSize)" << std::endl;
        }

        if ( temp != NULL ) {
            this->pointer = temp;
            this->size = newSize;
        } else {
            throw OutOfMemoryException();
        }
    }

public:
    ArrayList(int size) {
        this->pointer = (int*)malloc(sizeof(int) * size);
        this->current = 0;
        this->size = size;

        if ( DEBUG ) {
            std::cout << "ArrayList(int size)" << std::endl;
        }
    }

    void report() const {
        std::cout << "-----------" << std::endl;
        std::cout << "Pointer is: " << this->pointer << std::endl;
        std::cout << "Size is: " << this->size << std::endl;
        std::cout << "Current is: " << this->current << std::endl;
        std::cout << "-----------" << std::endl;
    }

    void print() const {
        int last = this->current - 1;

        for ( int i = 0; i < last; i++ ) {
            std::cout << this->pointer[i] << ' ';
        }

        std::cout << this->pointer[last] << std::endl;
    }

    // check const
    void fillByZero() const {
        for ( int i = 0; i < this->size; i++ ) {
            this->pointer[i] = 0;
        }

        if ( DEBUG ) {
            std::cout << "fillByZero()" << std::endl;
        }
    }

    void add(int value, int amount) {
        int newSize = this->current + amount;

        if ( newSize > this->size ) {
            this->expand(newSize);
        }

        for ( int i = 0; i < amount; i++ ) {
            this->pointer[this->current] = value;
            this->current += 1;
        }

        if (DEBUG) {
            std::cout << "add(int value, int amount)" << std::endl;
        }
    }
    void clear() {
        this->fillByZero();
        this->current = 0;
    }

    int& getByIndex(int index) const {
        if ( index > current ) {
            throw NotInRangeException();
        }

        return this->pointer[index];
    }

    int& operator[](int index) const {
        return this->getByIndex(index);
    }

    int getElementsAmount() const {
        return this->current - 1;
    }

    int operator*() const {
        return this->getElementsAmount();
    }

    int count(int element) const {
        int counter = 0;

        for ( int i = 0; i < this->current; i++ ) {
            if ( this->pointer[i] == element ) {
                counter += 1;
            }
        }

        return counter;
    }

    ~ArrayList() {
        free(this->pointer);
        this->size = 0;
        this->pointer = NULL;

        if (DEBUG) {
            std::cout << "~ArrayList()" << std::endl;
        }
    }
};

std::ostream& operator<<(std::ostream& os, const ArrayList& list) {
    for ( int i = 0; i < *list; i++ ) {
        os << list[i] << ' ';
    }

    os << list[*list] << std::endl;

    return os;
}
