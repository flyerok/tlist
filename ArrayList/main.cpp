#include <iostream>
#include "ArrayList.cpp"

int main() {
    ArrayList* list_p = new ArrayList(10);
    ArrayList& list = *list_p;

    try {
        list.add(5, 7);
    } catch (OutOfMemoryException ex) {
        std::cout << "Ololo" << std::endl;
    }
    
    list.report();

    std::cout << list[2] << std::endl;
    std::cout << *list << std::endl;
    std::cout << list.count(5) << std::endl;

    try {
        list[500] = 4;
    } catch (NotInRangeException ex) {
        list[5] = 4;
    }

    std::cout << list << std::endl;
    list.clear();

    delete list_p;

    return 0;
}
