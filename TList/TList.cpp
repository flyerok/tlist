#include <iostream>
#include <cstdlib>

class OutOfMemoryException {};
class NotInRangeException {};

template <typename Type>
class TList {
private:
    Type* pointer;
    size_t current;
    size_t size;

    void expand(size_t newSize) {
        Type* temp;
        newSize = newSize + this->size / 5;

        temp = (Type*)realloc(this->pointer, sizeof(Type) * newSize);

        if (DEBUG) {
            std::cout << "expand(int newSize)" << std::endl;
        }

        if ( temp != NULL ) {
            this->pointer = temp;
            this->size = newSize;
        } else {
            throw OutOfMemoryException();
        }
    }

public:
    TList(size_t size) {
        this->pointer = (Type*)malloc(sizeof(Type) * size);
        this->current = 0;
        this->size = size;

        if ( DEBUG ) {
            std::cout << "TList(int size)" << std::endl;
        }
    }

    void report() const {
        std::cout << "-----------" << std::endl;
        std::cout << "Pointer is: " << this->pointer << std::endl;
        std::cout << "Size is: " << this->size << std::endl;
        std::cout << "Current is: " << this->current << std::endl;
        std::cout << "-----------" << std::endl;
    }

    void print() const {
        size_t last = this->current - 1;

        for ( size_t i = 0; i < last; i++ ) {
            std::cout << this->pointer[i] << ' ';
        }

        std::cout << this->pointer[last] << std::endl;
    }

    // check const
    // void fillByZero() const {
    //     for ( size_t i = 0; i < this->size; i++ ) {
    //         this->pointer[i] = 0;
    //     }

    //     if ( DEBUG ) {
    //         std::cout << "fillByZero()" << std::endl;
    //     }
    // }

    void add(const Type& value, size_t amount) {
        size_t newSize = this->current + amount;

        if ( newSize > this->size ) {
            this->expand(newSize);
        }

        for ( size_t i = 0; i < amount; i++ ) {
            this->pointer[this->current] = value;
            this->current += 1;
        }

        if (DEBUG) {
            std::cout << "add(int value, int amount)" << std::endl;
        }
    }

    void clear() {
        // this->fillByZero();
        this->current = 0;
    }

    Type& getByIndex(size_t index) const {
        if ( index > current ) {
            throw NotInRangeException();
        }

        return this->pointer[index];
    }

    Type& operator[](size_t index) const {
        return this->getByIndex(index);
    }

    size_t getElementsAmount() const {
        return this->current - 1;
    }

    size_t operator*() const {
        return this->getElementsAmount();
    }

    size_t count(Type& element) const {
        size_t counter = 0;

        for ( size_t i = 0; i < this->current; i++ ) {
            if ( this->pointer[i] == element ) {
                counter += 1;
            }
        }

        return counter;
    }

    ~TList() {
        free(this->pointer);
        this->size = 0;
        this->pointer = NULL;

        if (DEBUG) {
            std::cout << "~TList()" << std::endl;
        }
    }
};

template <typename Type>
std::ostream& operator<<(std::ostream& os, const TList<Type>& list) {
    for ( int i = 0; i < *list; i++ ) {
        os << list[i] << ' ';
    }

    os << list[*list] << std::endl;

    return os;
}
