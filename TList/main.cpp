#include <iostream>
#include "TList.cpp"
// #include "../../../Point/Point.hpp"
#include "../../../Vector.cpp"

int main() {
    TList<Vector>* list_p = new TList<Vector>(10);
    TList<Vector>& list = *list_p;
    Vector a = Vector(1.1, 2.2);
    Vector b = Vector(1.5, 2.2);

    try {
        list.add(a, 7);
    } catch (OutOfMemoryException ex) {
        std::cout << "Ololo" << std::endl;
    }
    
    list.report();
    list[3] = b;
    list[4] = b;
    std::cout << list[2] << std::endl;
    std::cout << *list << std::endl;
    std::cout << list.count(b) << std::endl;

    try {
        list[500] = 4;
    } catch (NotInRangeException ex) {
        list[5] = 4;
    }

    std::cout << list << std::endl;
    list.clear();

    delete list_p;

    return 0;
}
